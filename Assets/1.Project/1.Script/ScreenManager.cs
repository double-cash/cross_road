﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenManager : MonoBehaviour
{

    public Vector2 ScreenSize = new Vector2(1024, 768);
    public Vector2 screen;
    public GameObject Q_page, T_page, F_page;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Screen.width != ScreenSize.x || Screen.height != ScreenSize.y)
        {
            ChangeScreenSize();
        }

    }

    public void ChangeScreenSize(float x = 1024,float y = 768)
    {

        ScreenSize = new Vector2(Screen.width, Screen.height);

        float xy = ScreenSize.x / ScreenSize.y;
        float yx = ScreenSize.y / ScreenSize.x;

        screen = new Vector2(x, y);

        if ((x / y) != xy)
        {

            if (ScreenSize.x > ScreenSize.y)
            {
                if (x > y)
                {
                    if ((y / x) > yx)
                        screen = new Vector2(ScreenSize.y * (x / y), ScreenSize.y);
                    else if ((y / x) > yx)
                        screen = new Vector2(ScreenSize.x, ScreenSize.x * (y / x));
                    else
                        screen = ScreenSize;
                }
                else if (x < y)
                {
                    screen = new Vector2(ScreenSize.y * (x / y), ScreenSize.y);
                }
                else
                {
                    screen = new Vector2(ScreenSize.y * (x / y), ScreenSize.y);
                }
            }
            else if (ScreenSize.x < ScreenSize.y)
            {
                if (x > y)
                {
                    screen = new Vector2(ScreenSize.x, ScreenSize.x * (y / x));
                }
                else if (x < y)
                {
                    if ((x / y) > xy)
                        screen = new Vector2(ScreenSize.y * (x / y), ScreenSize.y);
                    else if ((x / y) < xy)
                        screen = new Vector2(ScreenSize.x, ScreenSize.x * (y / x));
                    else
                        screen = ScreenSize;
                }
                else
                {
                    screen = new Vector2(ScreenSize.x, ScreenSize.x * (y / x));
                }
            }
            else
            {
                if (x > y)
                    screen = new Vector2(ScreenSize.x, ScreenSize.x * (y / x));
                else if (x < y)
                    screen = new Vector2(ScreenSize.y * (x / y), ScreenSize.y);
                else
                    screen = ScreenSize;
            }

        }
        else
        {
            screen = ScreenSize;
        }

        Q_page.GetComponent<RectTransform>().sizeDelta = screen;
        T_page.GetComponent<RectTransform>().sizeDelta = screen;
        F_page.GetComponent<RectTransform>().sizeDelta = screen;

    }

    public void A_btn(bool truefalse)
    {
        Q_page.SetActive(false);

        if (truefalse)
        {
            T_page.SetActive(true);
            F_page.SetActive(false);
        }
        else
        {
            T_page.SetActive(false);
            F_page.SetActive(true);
        }

    }

    public void B_btn()
    {
        Q_page.SetActive(true);
        T_page.SetActive(false);
        F_page.SetActive(false);
    }

}
